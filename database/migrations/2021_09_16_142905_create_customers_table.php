<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();

            // paket wedding
            $table->string('paket_wedding');
            $table->string('demo');
            $table->string('url_domain');
            $table->string('sub_domain');
            $table->string('music');

            // data mempelai pria
            $table->string('nama_lengkap_pria');
            $table->string('nama_panggilan_pria');
            $table->string('putra_ke_pria');
            $table->string('instagram_pria');
            $table->string('gambar_pria');

            // data mempelai wanita
            $table->string('nama_lengkap_wanita');
            $table->string('nama_panggilan_wanita');
            $table->string('putra_ke_wanita');
            $table->string('instagram_wanita');
            $table->string('gambar_wanita');

            // data orang tua pria
            $table->string('ayah_pria');
            $table->string('ibu_pria');

            // data orang tua wanita
            $table->string('ayah_wanita');
            $table->string('ibu_wanita');

            //detail acara
            $table->date('tanggal_akad');
            $table->time('jam_akad');
            $table->date('tanggal_resepsi');
            $table->time('jam_resepsi');
            $table->text('alamat_akad');
            $table->text('alamat_resepsi');
            $table->text('quotes');
            $table->text('catatan_lain');

            //rekanan
            $table->string('rekanan');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
