<?php

namespace App\Http\Controllers;

use App\Models\Basic;
use App\Models\Premium;
use App\Models\PaketWedding;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home(Request $request){
        
        $demo = $request->demo;
        $premium = $request->premium;
        // dd($premium);

        return view('home', [
            'demo' => $demo,
            'premium' => $premium,
            'title' => 'DWP'
        ]);
    }

    public function form(Request $request)
    {
        if($request->paket){
            $result = PaketWedding::where('jenis_paket', $request->paket)->pluck('demo_template');

            return response()->json($result);
        }

        $data = PaketWedding::all();

        return view('form',[
            'data' => $data
        ]);
    }

    public function design()
    {
        return view('basic-design',[
            'title' => 'Basic Design'
        ]);
    }

    public function premiumDesign()
    {
        return view('premium-design',[
            'title' => 'Premium Design'
        ]);
    }
}
